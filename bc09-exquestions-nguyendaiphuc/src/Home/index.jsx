import React, { Component } from "react";
import { connect } from "react-redux";
import FillInBlank from "../component/questionItem/FillInBlank";
import MultipleChoice from "../component/questionItem/MultipleChoice";
import { fetchQuestion } from "../store/action/question";
class Home extends Component {
  handleSubmit = () => {
    let result = 0;
    this.props.answerList.filter((answer) => {
      if (answer.answer.exact) {
        result++;
      }
    });
    alert(`Bạn đã đúng : ${result}/${this.props.questionList.length} câu`);
  };
  render() {
    return (
      <div>
        <h1 className="text-center m-3 text-warning border-bottom">Bài Thi</h1>
        {this.props.questionList.map((item) => {
          const { questionType } = item;
          console.log(questionType);
          return (
            <div key={item.id}>
              {questionType === 1 ? (
                <MultipleChoice item={item} />
              ) : (
                <FillInBlank item={item} />
              )}
            </div>
          );
        })}
        <button
          onClick={this.handleSubmit}
          className="btn btn-success m-3 text-center"
        >
          Nộp Bài
        </button>
      </div>
    );
  }

  async componentDidMount() {
    /// Gửi dispatch lên middwere để xử lý call api
    this.props.dispatch(fetchQuestion);
  }
}
const mapStateToProps = (state) => {
  return {
    questionList: state.questions.questionList,
    answerList: state.questions.answerList,
  };
};

export default connect(mapStateToProps)(Home);
