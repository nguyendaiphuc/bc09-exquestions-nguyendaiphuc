import { actionType } from "../action/type";

const initialState = {
  questionList: [],
  answerList: [],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_QUESTIONS:
      state.questionList = action.payload;
      return { ...state };
    case actionType.ANSWER:
      const cloneAnswerList = [...state.answerList];
      const foundAnswer = cloneAnswerList.findIndex(
        (answer) => answer.questionId === action.payload.questionId
      );
      if (foundAnswer === -1) {
        const newAnswer = { ...action.payload };
        cloneAnswerList.push(newAnswer);
      } else {
        cloneAnswerList[foundAnswer].answer = action.payload.answer;
      }
      state.answerList = cloneAnswerList;
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
