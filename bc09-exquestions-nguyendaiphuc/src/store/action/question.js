import axios from "axios";
import { actionType } from "./type";
import { createAction } from "./index";

export const fetchQuestion = async (dispatch) => {
  //side-effect:những hành động thay đổi 1 state nằm ngoài scope của function
  try {
    const res = await axios({
      method: "GET",
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions",
    });
    dispatch(createAction(actionType.SET_QUESTIONS, res.data));
    console.log(res);
  } catch (err) {
    console.log(err);
  }
};
