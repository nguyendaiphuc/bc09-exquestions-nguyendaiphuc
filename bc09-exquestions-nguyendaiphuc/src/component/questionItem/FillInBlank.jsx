import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../../store/action";
import { actionType } from "../../store/action/type";
class QuestionItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {},
    };
  }

  handleChange = (event, answers) => {
    this.setState(
      {
        formValue: {
          questionId: event.target.id,
          answer: {
            content: answers[0].content,
            exact:
              answers[0].content.toLowerCase() ===
              event.target.value.toLowerCase()
                ? true
                : false,
          },
        },
      },
      () => {
        this.props.dispatch(
          createAction(actionType.ANSWER, this.state.formValue)
        );
        console.log(this.state.formValue);
      }
    );
  };

  render() {
    const { id, content, answers } = this.props.item;

    return (
      <div className="ml-4">
        <h5 className="text-info  mb-3">
          Câu Hỏi {id}: {content}
        </h5>
        <input
          onBlur={(event) => this.handleChange(event, answers)}
          className="ml-3 w-50 form-control"
          type="text"
          id={id}
        />
      </div>
    );
  }
}

export default connect()(QuestionItem);
