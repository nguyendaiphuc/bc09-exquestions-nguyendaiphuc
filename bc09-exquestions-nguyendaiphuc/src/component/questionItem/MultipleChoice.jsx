import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../../store/action";
import { actionType } from "../../store/action/type";
import {
  FormControl,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio,
} from "@material-ui/core";
class MultipleChoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {},
    };
  }
  handleChange = (id, answer) => {
    this.setState(
      {
        formValue: {
          questionId: id,
          answer: {
            content: answer.content,
            exact: answer.exact,
          },
        },
      },
      () => {
        this.props.dispatch(
          createAction(actionType.ANSWER, this.state.formValue)
        );
        console.log(id);
      }
    );
  };
  renderMaterialRadio = () => {
    const { id, content, answers } = this.props.item;
    return (
      <FormControl component="fieldset">
        <FormLabel component="legend">
          {id} {content}
        </FormLabel>
        <RadioGroup
          aria-label="gender"
          name={content}
          value={answers}
          onChange={this.handleChange}
        >
          {answers.map((item) => (
            <div key={item.id}>
              <FormControlLabel
                value={item.content}
                control={<Radio />}
                label={item.content}
              />
            </div>
          ))}
        </RadioGroup>
      </FormControl>
    );
  };
  render() {
    const { id, content, answers } = this.props.item;
    return (
      <div className="ml-4">
        <h5 className="text-info">
          Câu Hỏi {id}: {content}
        </h5>

        {answers.map((answer) => (
          <div key={answer.id}>
            <input
              onChange={() => this.handleChange(id, answer)}
              type="radio"
              value={answer.content}
              name={id}
            />
            <label>{answer.content}</label>
          </div>
        ))}
      </div>
    );
  }
}

export default connect()(MultipleChoice);
